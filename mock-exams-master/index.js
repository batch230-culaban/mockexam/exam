// Concepts and Theory Exam:
// https://forms.gle/GapYaHb6LWkNHw1Z6 


// Mock Technical Exam (Function Coding)
// No need to create a test inputs, the program automatically checks any input through its parameter/s
// Use the parametrs as your test input
// Make sure to use return keyword so your answer will be read by the tester (Example: return result;);
// - Do not add or change parameters
// - Do not create another file
// - Do not share this mock exam

// Use npm install and npm test to test your program

function countLetter(letter, sentence) {
    let result = 0;
    
    if(letter.length !== 1){
        return undefined;
    }

    for(let i=0; i<sentence.length; i++){
        if(sentence[i] === letter){
            result++
        }
    }
    return result;
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
}


function isIsogram(text) {

    text = text.toLowerCase();
    const letterCounts = {};

    for(let i=0; i<text.length; i++){
        const letter = text[i]
    
            if(letterCounts[letter]){
            return false;
                }
                    letterCounts[letter] = 1;
                        }
                            return true;
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.   
}

function purchase(age, price) {

    if(age < 13){
        return undefined;
    }
    else if(age >= 13 && age <= 21  || age >= 60){
        let discountedPrice = price*0.8;
        return discountedPrice.toFixed(2);
    }
    else {
        return price.toFixed(2);
    }
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {

    const hotCategories = [];

    for(let i=0; i < items.length; i++){
        const category = items[i].category;
        const stocks = items[i].stocks;

        if (stocks === 0 && hotCategories.indexOf(category) === -1){
            hotCategories.push(category);
        }
    }
    return hotCategories;
}

    // The passed items array from the test are the following:
    const items = [
    { 
        id: 'tltry001', 
        name: 'soap', 
        stocks: 14, 
        category: 'toiletries' 
    },
    {   id: 'tltry002', 
        name: 'shampoo', 
        stocks: 8, 
        category: 'toiletries' 
    },
    {   id: 'tltry003', 
        name: 'tissues', 
        stocks: 0, 
        category: 'toiletries' 
    },
    {   id: 'gdgt001', 
        name: 'phone', 
        stocks: 0, 
        category: 'gadgets' 
    },
    {   id: 'gdgt002', 
        name: 'monitor', 
        stocks: 0, 
        category: 'gadgets' 
    }];
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.
    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.


function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.
    // The passed values from the test are the following:
    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    const flyingVoters = [];

    for(let i=0; i < candidateA.length; i++){
        const voter = candidateA[i];
        if(candidateB.includes(voter)){
            flyingVoters.push(voter)
        }
    }
    return flyingVoters;
};

const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB =  ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};